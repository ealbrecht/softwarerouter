/**-----------------------------------------------
 * @author: Edward L Albrecht
 * CMSC 506 - Computer Networking Software Router
 * Class: Server
 ----------------------------------------------**/

package server;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

import packet.Packet;

/**----------------------------------------------
 * @author Edward L Albrecht
 * A Server object contains a ServerSocket that nodes will connect to
 * A Socket to send information out to the node.
 * A server relies on the nodes to properly forward it's message to the destination
 ----------------------------------------------**/
public class Server2 implements Runnable{
	DatagramSocket server = null;
	ObjectOutputStream out;
	ObjectInputStream in;
	Object message;
	Packet pkt;
	String src;
	int PORT;
	 
	/** @author Edward L Albrecht
	 * Method:	Server2(...)
	 * Purpose:	Server Constructor
	 * Input:	The String name and int port for this
	 * 			Server object
	 * Output:	None
	 --------------------------------------------*/
	public Server2(String name, int p){
		this.src = name;
		this.PORT = p;
	}
	/**--------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	run()
	 * Purpose:	Main execution method
	 * Input:	None
	 * Output:	None
	 --------------------------------------------*/
	public void run(){
		try {
			boolean run = true;
			server = new DatagramSocket(this.PORT);
			System.out.println("Running on Port: "+server.getLocalPort());
			while(run){
				byte[] buf = new byte[3680];
				DatagramPacket packet = new DatagramPacket(buf, buf.length);
				server.receive(packet);
				
				//display response
				buf = packet.getData();
				Packet pack = deserialize(buf);
				System.out.println(pack.getSrc()+"> "+pack.getMsg());
				
				//send response to src
				pack = new Packet(src, pack.getSrc(),0, "Hello "+pack.getSrc());
				buf = serialize(pack);

				InetAddress address = packet.getAddress();
				int port = packet.getPort();
				packet = new DatagramPacket(buf, buf.length, address, port);
				server.send(packet);
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}//end run
	/**----------------------------------------
	 * @author Edward L Albrecht
	 * Method:	serialize(...)
	 * Purpose:	Serialize a packet object into
	 * 			a byte[] array to be used in
	 * 			transmission
	 * Input:	The Packet object to be converted
	 * Output:	The byte[] representation of this
	 * 			Packet
	 -----------------------------------------*/
	public byte[] serialize(Packet pack) throws IOException{
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(pack);
		return b.toByteArray();
	}
	/**----------------------------------------
	 * @author Edward L Albrecht
	 * Method:	deserialize(....)
	 * Purpose:	Deserialize a byte[] array back into
	 * 			a Packet object, to see what the
	 * 			message received is.
	 * Input:	The byte[] array to be converted
	 * Output:	A new Packet object
	 -----------------------------------------*/
	public Packet deserialize(byte[] bytes) throws ClassNotFoundException, IOException{
		ByteArrayInputStream b = new ByteArrayInputStream(bytes);
		ObjectInputStream o = new ObjectInputStream(b);
		return (Packet) o.readObject();
	}
	/**----------------------------------------
	 * @author Edward L Albrecht
	 * Method:	main(String[] args)
	 * Purpose:	Class execution
	 * Input:	A String[] array of arguments
	 * 			from the command line. NOT USED
	 * Output:	None
	 -----------------------------------------*/
	public static void main(String[] args){
		Server2 server = new Server2("Server2", 4452);
		server.run();
	}
}
