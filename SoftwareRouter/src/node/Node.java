/**------------------------------------------------
 * @author: Edward L Albrecht
 * CMSC 506 - Computer Networking Software Router
 * Class: Node
 -----------------------------------------------**/
package node;

import java.io.*;
import java.util.*;
import java.net.*;
import link.Link;
import packet.Packet;

/**----------------------------------------------
 * @author Edward L Albrecht
 * A node object contains a ServerSocket that clients will connect to
 * A Socket to send information out to the Client on a dedicated connection to this node
 * A Socket to create connections when it needs to forward a message.
 ----------------------------------------------**/
public class Node implements Runnable{
	static DatagramSocket server;
	static DatagramPacket dgp;
	static Packet packet;
	BufferedReader in = null;
	DatagramSocket outConn;
	Object message;
	static String src;
	String dest = "";
	static int PORT;
	static Link table[][];
	static ArrayList<Link> neighbor;
	static Random rng = new Random();
	boolean tableChange;
	byte[] buf = new byte[3680];
	
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	Node(String name, int p)
	 * Purpose:	Node constructor
	 * Input:	a String and an integer, to be used
	 * 			as the name and port for this node
	 * Output:	None
	 --------------------------------------------*/
	public Node(String name, int p){
		 this.src= name;
		 this.PORT = p;
		 table =  new Link[9][9];
		 this.neighbor = new ArrayList<Link>();
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method: run()
	 * Purpose: When a thread is run, it will
	 * 			process the packets as they come in
	 * Input:	None
	 * Output:	None
	 --------------------------------------------*/	
	public void run(){
		try {
			processPacket(packet, buf, dgp);
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		//place these packs into a queue for processing
	}//end run
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method: processPacket(...)
	 * Purpose: Determines what the packet received is,
	 * 			and then handles it.
	 * Input:	A Packet object, a byte[] array, and the
	 * 			DatagramPacket
	 * Output:	None
	 --------------------------------------------*/
	public void processPacket(Packet pack, byte[] buf, DatagramPacket dgp) throws ClassNotFoundException, IOException, InterruptedException{
		//check to see where this is going
		if(pack.getDest().equalsIgnoreCase(this.src))
		{	//else this is the packet's destination
			//determine type of packet
			if(pack.getType() == 0){
				if(pack.getType() == 0){
					if(pack.getMsg().equalsIgnoreCase("Table Request")){
						respTblReq(src, pack.getSrc(), getHopPort(pack.getSrc()), table);
					}
					else{
						//routine packet, display a response and send one back to client
						System.out.println(pack.getSrc()+"> "+pack.getMsg());
						//send a reply back
						sendMessage(pack.getSrc(), getName()+" received message", getHopPort(pack.getSrc()));
						//System.out.println("Sent reply");
					}
				}
			}
			else
			{
				//else this is a table update, process table update
				System.out.println(getName()+"> Received table update from: "+pack.getSrc());
				//System.out.println("CURRENT TABLE");
				//printTable(table);
				//System.out.println("RECEIVED TABLE");
				//printTable(pack.getTable());
				tableChange = updateTable(pack.getSrc(), pack.getTable());
				if(tableChange){
					System.out.println(getName()+"> Routing table has been changed.  Sending to neighbors.");
					//printTable(table);
					//table has been changed, send new table to neighbors
					sendTableUpdate();
					//printTable();
				}
				else{
					System.out.println("RCV'D TABLE NOT SUFFICIENT FOR UPDATE");
				}
			}
		}//end if
		else if(!pack.getDest().equalsIgnoreCase(src)){
			fwdMessage(pack);
		}
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method: sendTblReq()
	 * Purpose: sends a request to all neighbors
	 * 			to provide this node with neighbor table
	 * Input:	None
	 * Output:	None
	 --------------------------------------------*/
	public static void sendTblReq() throws IOException{
		for(Link lnks: neighbor){
			if(lnks.getDest().equalsIgnoreCase("Client1")
					||lnks.getDest().equalsIgnoreCase("Client2")
					||lnks.getDest().equalsIgnoreCase("Server1")
					||lnks.getDest().equalsIgnoreCase("Server2")){
			}
			else{
				Packet packet = new Packet(getName(), lnks.getDest(), 0, "Table Request");
				byte[] buf = serialize(packet);
				DatagramPacket dgp = new DatagramPacket(buf, buf.length, 
						InetAddress.getByName(lnks.getAddr()), lnks.getPort());
				server.send(dgp);
			}
		}
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method: respTblReq(...)
	 * Purpose: Respond to the table request by
	 * 			creating a new packet to send to the
	 * 			source node
	 * Input:	A String source, String destination,
	 * 			int port, and the Link[][] table
	 * Output:	None
	 --------------------------------------------*/
	public void respTblReq(String src, String destination, int p, Link[][] table){
		Packet temp = new Packet(src, destination,1, table);
		try {
			byte[] buf = serialize(temp);
			InetAddress address = InetAddress.getByName("localhost");
			DatagramPacket packet = new DatagramPacket(buf, buf.length, address, p);
			server.send(packet);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	fwdMessage()
	 * Purpose:	Will determine the packet destination,
	 * 			and then forward the packet to the next hop.
	 * Input: 	A Packet object
	 * Output:	None
	 --------------------------------------------*/
	public void fwdMessage(Packet pack){
		//Packet destination is not this node
		//check to see if destination is reachable via this node
		int routePort = getHopPort(pack.getDest());
		System.out.println("Packet Destination: "+pack.getDest());
		System.out.println("Reachable? "+isReachable(pack.getDest()));
		System.out.println("Next Hop Port: "+routePort);
		try {
			sendMessage(pack, routePort);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Forwaded message to: "+getHop(routePort)+" on port: "
				+getHopPort(pack.getDest()));
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method: sendMessage(...)
	 * Purpose: Send this packet to this node
	 * Input:	A Packet object and an int port
	 * Output:	None
	 --------------------------------------------*/
	public void sendMessage(Packet pack, int p) throws IOException{
		InetAddress address = null;
		for(Link lnks: neighbor){
			if(lnks.getPort() == p){
				address = InetAddress.getByName(lnks.getAddr());
			}
		}
		byte[] buf = serialize(pack);
		DatagramPacket dgp = new DatagramPacket(buf, buf.length, address, p);
		server.send(dgp);
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	sendMessage(....)
	 * Purpose:	Send a message to the destination
	 * Input:	String destination, String message, and
	 * 			the int port for the next hop
	 * Output:	None
	 --------------------------------------------*/
	public static void sendMessage(String dest, String msg, int p) throws IOException, ClassNotFoundException{
		byte[] buf = new byte[3680];				//instantiate buffer byte array
		Packet temp = new Packet(src, dest, 0, msg);//Create a new packet, using this source, the destination provided
												//and the message
		buf = serialize(temp);					//serialize this packet object into a byte array
		InetAddress address = InetAddress.getByName("localhost");	//get the address
		DatagramPacket packet = new DatagramPacket(buf, buf.length, address, p);	//create a new datagramPacket.  
												//The last int is the port to send this packet, and in this case
												//this is always node 1 for client 1
		server.send(packet);				//send this packet through the socket
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	sendTableUpdate()
	 * Purpose: Send the table to all neighbors
	 * Input:	None
	 * Output:	None
	 --------------------------------------------*/
	public static void sendTableUpdate() throws IOException{
		//Need to send the updated table to every neighbor of this node
		//System.out.println("Made it here");
		//System.out.println("Neighbor Size: "+neighbor.size());
		Packet packet;
		for(Link lnks: neighbor){
			if(lnks.getDest().equalsIgnoreCase("Client1")
					||lnks.getDest().equalsIgnoreCase("Client2")
					||lnks.getDest().equalsIgnoreCase("Server1")
					||lnks.getDest().equalsIgnoreCase("Server2")){
			}
			else{
				//To create a packet to send, need source, a destination, type will be 1, and the routingtable
				packet = new Packet(getName(), lnks.getDest(), 1, table);
				byte[] buf = serialize(packet);
				//System.out.println(buf.length);
				//System.out.println("Packet serialized");
				DatagramPacket dgp = new DatagramPacket(buf, buf.length, 
						InetAddress.getByName(lnks.getAddr()), lnks.getPort());
				//System.out.println("DGP created");
				//now that the datagrampacket is created, send it out
				server.send(dgp);
				System.out.println("Sent a table update to: "+lnks.getDest());
			}
		}
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	initTable()
	 * Purpose:	Initializes the table
	 * Input:	None
	 * Output:	None
	 --------------------------------------------*/
	public static void initTable(){
		for(int i = 0; i < table.length; i++){
			for(int j = 0; j < table[i].length; j++){
				table[i][j] = new Link();
			}
		}
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	neighborSetup()
	 * Purpose:	Initialize the neighbor arraylist
	 * Input:	The table
	 * Output:	None
	 --------------------------------------------*/
	public static void neighborSetup(Link[][] table){
		int loc = getMyTableLocation();
		for(int x = 0; x < table.length; x++){
			//now that we have this nodes location in the routing table
			//check to see if any destinations are linked to this node
			if(table[loc][x].getCost() == 1 && 
					(!table[loc][x].getName().equalsIgnoreCase("Client1")
							||!table[loc][x].getName().equalsIgnoreCase("Client2")
							||!table[loc][x].getName().equalsIgnoreCase("Server1")
							||!table[loc][x].getName().equalsIgnoreCase("Server2") )
			){
				//this entry is a neighbor
				System.out.println(table[loc][x].toString());
				neighbor.add(table[loc][x]);	//add this link to the neighbor list
			}
		}
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	tableSetup()
	 * Purpose:	Setup the table from the file
	 * Input:	A string filename
	 * Output:	None
	 --------------------------------------------*/
	public static void tableSetup(String filename) throws IOException{
		initTable();
		FileReader fr = new FileReader(filename);
		BufferedReader br = new BufferedReader(fr);
		for(int i = 0; i < 9; i++){
			String row = br.readLine();
			//System.out.println(row);
			String[] token = row.split("[;]");
			for(int j = 0; j < 9; j++){
				//System.out.println(token[j]);
				String[] item = token[j].split("[,]");
				//System.out.println(item[0]+"->"+item[1]+"@"+item[2]+":"+Integer.parseInt(item[3])+"="
									//+Integer.parseInt(item[4]));
				table[i][j] = new Link(item[0], item[1], item[2], Integer.parseInt(item[3]), Integer.parseInt(item[4]));
			}
		}
		br.close();
		neighborSetup(table);
		//System.out.println("ITEM 0: "+item[0]+"ITEM 1: "+item[1]+"ITEM 2: "+item[2]+"ITEM 3: "+item[3]);
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	updateTable()
	 * Purpose:	When a table is received, check to see
	 * 			if any cheaper routes are available
	 * Input:	String source and the Link[][] updateTable sent
	 * Output:	A boolean value, used to determine if
	 * 			the table has been altered.
	 --------------------------------------------*/
	public boolean updateTable(String source, Link[][] updateTable) throws ClassNotFoundException, IOException{
		boolean UPDATE = false;
		int myLoc = getMyTableLocation();
		int srcLoc = getSenderLocation(source);
		int sPort = getNeighborPort(source);
		for(int i = 0; i < table.length; i++){
			//compare the row I exist on against the sender row
			//System.out.println("og: "+table[myLoc][i]);
			//System.out.println("cp: "+updateTable[srcLoc][i]);
			if(table[myLoc][i].getCost() == 0){
				//This means, looking at self
			}
			if(table[myLoc][i].getPort() == sPort){
				//this hop has already been added
			}
			else if(table[myLoc][i].getCost() > updateTable[srcLoc][i].getCost()
					&& table[srcLoc][i].getCost() != 0){
				//there is a cheaper route using this source
				//update this cost
				updateLink(myLoc, i, updateTable[srcLoc][i].getCost(), sPort);
				updateLink(i, myLoc, updateTable[srcLoc][i].getCost(), sPort);
				//make this the hop & hops can only be to this node
				//additionally, need update all of this nodes neighbors indicating that this node
				//can reach destination via this cost
				updateNeighNodes(myLoc, i);
				
				UPDATE = true;
			}//end else
		}//end for
		
		return UPDATE;
	}//end updateTable
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getNeighborPort()
	 * Purpose:	Based on the String name, check through
	 * 			the neighbor arraylist, and return back
	 * 			its port
	 * Input:	A String name
	 * Output:	An int port
	 --------------------------------------------*/
	public int getNeighborPort(String name){
		int dPort = -1;
		for(Link lnk:neighbor){
			if(lnk.getDest().equalsIgnoreCase(name)){
				dPort = lnk.getPort();
			}
		}
		return dPort;
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getHop()
	 * Purpose:	Based on the int provided, return
	 * 			the String name of this hop
	 * Input:	An int port
	 * Output:	A String name
	 --------------------------------------------*/
	public String getHop(int p){
		String name = "";
		for(Link lnk:neighbor){
			if(lnk.getPort() == p){
				name = lnk.getDest();
			}
		}
		return name;
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getHopPort()
	 * Purpose: Checks through the table and gets
	 * 			the next hop to the destination
	 * Input:	A String name for the destination
	 * Output:	An int port for the next hop
	 --------------------------------------------*/
	public int getHopPort(String name){
		int hPort = -1;
		int loc = getMyTableLocation();
		//find the port that is next in the hop
		for(int i = 0; i < table.length; i++){
			if(table[loc][i].getDest().equalsIgnoreCase(name)){
				hPort = table[loc][i].getPort();
			}
		}
		return hPort;
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	updateLink()
	 * Purpose:	Update the table entry for a new
	 * 			cost and hop
	 * Input:	An int row, int col, int cost, and int port
	 * Output:	None
	 --------------------------------------------*/
	public void updateLink(int row, int col, int cost, int p){
		System.out.println("--------UPDATE------------");
		System.out.println(table[row][col].toString());
		table[row][col].updateCost(cost+1);
		table[row][col].updatePort(p);
		//verify new cost put in
		System.out.println(table[row][col].toString());
		System.out.println("--------------------------");
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	updateNeighNodes()
	 * Purpose:	For every neighbor for this node,
	 * 			update the cost and hop, to reflect
	 * 			that this node can reach the destination
	 * Input:	An integer for this nodes location in the table,
	 * 			and an integer for which neighbor to look at
	 * Output:	None
	 --------------------------------------------*/
	public void updateNeighNodes(int mLoc, int i){
		for(Link lnk: neighbor){
			//for every neighbor get name to get table location
			int nLoc = getSenderLocation(lnk.getDest());
			//now that we have its table location, go to that entry and check to see
			//if new cost is less than entry cost
			//System.out.println("NEIGH: "+ table[nLoc][i].toString());
			//System.out.println("MY: "+ table[mLoc][i].toString());
			if(table[nLoc][i].getCost() > table[mLoc][i].getCost()+1){
				//now we update the entry
				updateLink(nLoc, i, table[mLoc][i].getCost(), PORT);
				//updateLink(i, nLoc, table[mLoc][i].getCost(), table[mLoc][i].getPort());
			}//end if
		}//end for
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	printTable()
	 * Purpose:	Output the table to the screen
	 * Input:	A Link[][] table object
	 * Output:	None
	 --------------------------------------------*/
	public static void printTable(Link[][] tbl){
		for(int i = 0; i < tbl.length; i++){
			for(int j = 0; j < tbl[i].length; j++){
				System.out.print(tbl[i][j].toString()+ "\t ");
			}
			System.out.println("");
		}
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method: isReachable
	 * Purpose:	Check the table to see if this destination
	 * 			can be reached via this node
	 * Input:	A string destination
	 * Output:	A boolean value used to determine if
	 * 			the node is reachable or not
	 --------------------------------------------*/
	public boolean isReachable(String name){
		boolean verdict = false;
		int loc = getMyTableLocation();
		for(int i = 0; i < table.length; i++){
			if(table[loc][i].getDest().equalsIgnoreCase(name)){
				if(table[loc][i].getCost() != 99){
					verdict = true;
				}
			}
		}
		return verdict;
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getMyTableLocation()
	 * Purpose:	Searches the table for the row this
	 * 			node exists on
	 * Input:	None
	 * Output:	An integer value, that is the row
	 * 			this node exists on
	 --------------------------------------------*/
	public static int getMyTableLocation(){
		int location = -1;	
		for(int i = 0; i < table.length; i++){
			if(table[i][0].getName().equalsIgnoreCase(src)){
				location = i;
			}
		}
		return location;
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getSenderLocation()
	 * Purpose:	Get the row location this sender exists on
	 * 			to be used to compare costs and hops
	 * Input:	A string for the sender name
	 * Output:	The int row value this sender exists on,
	 * 			in the table
	 --------------------------------------------*/
	public int getSenderLocation(String name){
		int location = -1;	
		for(int i = 0; i < table.length; i++){
			if(table[i][0].getName().equalsIgnoreCase(name)){
				location = i;
			}
		}
		return location;
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getName()
	 * Purpose:	return this nodes name
	 * Input:	None
	 * Output:	The string name for this node
	 --------------------------------------------*/
	public static String getName(){
		return src;
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	serialize(...)
	 * Purpose:	Convert the Packet object into a byte[]
	 * 			array
	 * Input:	The Packet object to be converted
	 * Output:	The byte[] array representation of this
	 * 			packet
	 --------------------------------------------*/
	public static byte[] serialize(Packet pack) throws IOException{
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(pack);
		return b.toByteArray();
	}
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	deserialize(...)
	 * Purpose:	Convert the byte[] array back into a
	 * 			Packet object
	 * Input:	The byte[] array to be converted
	 * Output:	A new packet object
	 --------------------------------------------*/
	public static Packet deserialize(byte[] bytes) throws ClassNotFoundException, IOException{
		ByteArrayInputStream b = new ByteArrayInputStream(bytes);
		ObjectInputStream o = new ObjectInputStream(b);
		Packet temp = (Packet) o.readObject();
		return temp;
	}	
	/**-------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	main(String[] args)
	 * Purpose:	Execution method
	 * Input:	A string[] of arguments from the command
	 * 			line.  NOT USED
	 * Output:	None.
	 --------------------------------------------*/
	public static void main(String[] args){
		final Node node = new Node("N1", 2004);
		try {
			tableSetup("E:/Documents/git/SoftwareRouter/SoftwareRouter/routeTable.txt");
			server = new DatagramSocket(PORT);
			System.out.println(PORT);
			System.out.println("Running on Port: "+server.getLocalPort());
			Thread.sleep(rng.nextInt(3000));
			sendTblReq();
			while(true){
				byte[] buf = new byte[3680];
				dgp = new DatagramPacket(buf, buf.length);
				server.receive(dgp);
				buf = dgp.getData();
				packet = deserialize(buf);
				Thread thread = new Thread(){
					public void run(){
						node.run();
					}
				};
				thread.start();
			}
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		node.run();
	}
}
