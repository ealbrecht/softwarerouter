/**------------------------------------------------
 * @author: Edward L Albrecht
 * CMSC 506 - Computer Networking Software Router
 * Class: Packet
 -----------------------------------------------**/

package packet;

import java.io.Serializable;

import link.Link;

public class Packet implements Serializable{
	String src;
	String dest;
	String msg;
	StringBuffer sB;
	String route;
	int type;
	Link[][] table;
	
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	Packet(..., String message)
	 * Purpose:	The Packet constructor that contains a string
	 * 			message.  ALL STRING MESSAGES WILL BE OF
	 * 			INT TYPE = 0
	 * Input:	The String source node, destination node
	 * 			An int for the type of packet, and the
	 * 			String message
	 * Output:	None
	 ---------------------------------------------------*/
	public Packet(String source, String destination, int typ, String message){
		this.src = source;
		this.dest = destination;
		this.type = typ;
		this.msg = message;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	Packet(...., Link[][] tbl)
	 * Purpose:	The Packet constructor that contains the
	 * 			routing table.  ALL TABLE MESSAGES WILL BE
	 * 			OF INT TYPE = 1
	 * Input:	The String source node, destination node
	 * 			An int for the type of packet, and the
	 * 			Link[][] routing table
	 * Output:	None
	 ---------------------------------------------------*/
	public Packet(String source, String destination, int typ, Link[][] tbl){
		this.src = source;
		this.dest = destination;
		this.type = typ;
		this.table = tbl;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	add_hop(String n)
	 * Purpose:	Append the hop to this packet
	 * 			NOT IMPLEMENTED
	 * Input:	The String name of this node
	 * Output:	None
	 ---------------------------------------------------*/
	void add_hop(String n){
		sB.append(n);
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getMsg()
	 * Purpose:	Return the String message in this packet
	 * Input:	None
	 * Output:	A String that is the message contained in
	 * 			this packet
	 ---------------------------------------------------*/
	public String getMsg(){
		return this.msg;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getSrc()
	 * Purpose:	Get the node that send this packet
	 * Input:	None
	 * Output:	The String name of this source node
	 ---------------------------------------------------*/
	public String getSrc(){
		return this.src;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getDest()
	 * Purpose:	Get the node that this was sent to
	 * Input:	None
	 * Output:	The String name of the destination node
	 ---------------------------------------------------*/
	public String getDest(){
		return this.dest;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getType()
	 * Purpose:	Return the type of Packet
	 * Input:	None
	 * Output:	Returns either 0 or 1.  0 is used for
	 * 			String messages, and 1 for routing tables
	 ---------------------------------------------------*/
	public int getType(){
		return this.type;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getTable()
	 * Purpose:	Return the routing table contained in this
	 * 			Packet
	 * Input:	None
	 * Output:	A link[][] object that is the routing table
	 ---------------------------------------------------*/
	public Link[][] getTable(){
		return this.table;
	}
}
