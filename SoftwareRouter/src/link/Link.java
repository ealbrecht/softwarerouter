/**-------------------------------------------------
 * @author: Edward L Albrecht
 * CMSC 506 - Computer Networking Software Router
 * Class: Link
 * 
 -------------------------------------------------**/

package link;
import java.io.Serializable;

public class Link implements Serializable{
	String name;
	String destination;
	String address;
	int port;
	int cost;
	
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	Link()
	 * Purpose:	Link constructor, empty just used for initialization
	 * Input:	None
	 * Output:	None
	 ---------------------------------------------------*/
	public Link(){}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	Link(....)
	 * Purpose:	Link constructor
	 * Input:	String name, destination, address
	 * 			int port and int cost of this particular link
	 * Output:	None
	 ---------------------------------------------------*/
	public Link(String nme, String dest, String addr, int p, int c){
		this.name = nme;
		this.destination = dest;
		this.address = addr;
		this.port = p;
		this.cost = c;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getName()
	 * Purpose:	Return the source node for this link
	 * Input:	None
	 * Output:	The String name of this source node
	 ---------------------------------------------------*/
	public String getName(){
		return this.name;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getDest()
	 * Purpose:	Return the destination node for this link
	 * Input:	None
	 * Output:	The string name of this destination node
	 ---------------------------------------------------*/
	public String getDest(){
		return this.destination;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getAddr()
	 * Purpose:	Return the destination address for this
	 * 			link
	 * Input:	None
	 * Output:	The String name of this address
	 ---------------------------------------------------*/
	public String getAddr(){
		return this.address;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getPort()
	 * Purpose:	Return the hop port for this address
	 * Input:	None
	 * Output:	An int port for this hop
	 ---------------------------------------------------*/
	public int getPort(){
		return this.port;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	getCost()
	 * Purpose:	Return the cost of this link
	 * Input:	None
	 * Output:	An int cost to this destination
	 ---------------------------------------------------*/
	public int getCost(){
		return this.cost;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	updateCost(int n)
	 * Purpose:	Update the cost of this link
	 * Input:	An int for the new cost
	 * Output:	None
	 ---------------------------------------------------*/
	public void updateCost(int n){
		this.cost = n;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	updatePort(int n)
	 * Purpose:	Update the hop port for this link
	 * Input:	An int for the new hop port
	 * Output:	None
	 ---------------------------------------------------*/
	public void updatePort(int n){
		this.port = n;
	}
	/**---------------------------------------------------
	 * @author Edward L Albrecht
	 * Method:	toString()
	 * Purpose:	Return the string representation of this link
	 * Input:	None
	 * Output:	A string representation of this link
	 ---------------------------------------------------*/
	public String toString(){
		String s = "";
		s = this.name+"->"+this.destination+"@"+this.address+":"+this.port+"="+this.cost;
		return s;
	}
}
