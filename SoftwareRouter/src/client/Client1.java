/**----------------------------------------------
 * Author: Edward L Albrecht
 * CMSC 506 - Computer Networking Software Router
 * Class: Client
 --------------------------------------------**/

package client;
import java.io.*;
import java.util.*;
import java.net.*;

import packet.Packet;

/**----------------------------------------------
 * Each Client object contains a socket to the node it's connecting to
 * 	the input and output object streams
 * 	an Object message, a Packet, and it's source name.
 ----------------------------------------------**/
public class Client1 implements Runnable{
	DatagramSocket requestSocket;
	Object message;
	String src;
	int PORT;
	
	/**----------------------------------------
	 * @author Edward L Albrecht
	 * Method:	Client1()
	 * Purpose:	Client1 constructor
	 * Input:	The string name and int port for
	 * 			this client
	 * Output:	None
	 -----------------------------------------*/
	public Client1(String name, int p){
		this.src = name;
		this.PORT = p;
	}
	/**----------------------------------------
	 * @author Edward L Albrecht
	 * Method:	run()
	 * Purpose:	Main execution method
	 * Input:	None
	 * Output:	None
	 -----------------------------------------*/
	public void run(){

			//1. creating a socket to connect to the server
			try {
				requestSocket = new DatagramSocket(this.PORT);
				System.out.println("Running on Port: "+requestSocket.getLocalPort());
				Scanner scan = new Scanner(System.in);
				String msg = "";			
				while(!msg.equalsIgnoreCase("quit")){
					if(msg.equalsIgnoreCase("quit")){
						break;
					}
					
					System.out.println("Enter destination to send message: ");
					String dest = scan.nextLine();
					System.out.println("Enter message to send: ");
					msg = scan.nextLine();
					//System.out.println(buf.length);
					sendMessage(dest, msg);				
					
					byte[] buf = new byte[3680];
					DatagramPacket packet = new DatagramPacket(buf, buf.length);
					//get response
					packet = new DatagramPacket(buf, buf.length);
					requestSocket.receive(packet);

					//display response
					buf = packet.getData();
					Packet pack = deserialize(buf);
					if(pack.getType() != 1){
						//if this is NOT a table update request
						System.out.println(pack.getSrc()+"> "+pack.getMsg());
					}
					else{
						//Else it is a table update, ignore
					}
					
					System.out.println("Type 'quit' to quit: ");
					msg = scan.nextLine();
				}
				
			} catch (SocketException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	/**----------------------------------------
	 * @author Edward L Albrecht
	 * Method:	sendMessage(.....)
	 * Purpose:	Create a packet object and send it
	 * 			to the destination
	 * Input:	A string destination, and String message
	 * Output:	None
	 -----------------------------------------*/
	public void sendMessage(String dest, String msg) throws IOException, ClassNotFoundException{
		byte[] buf = new byte[3680];				//instantiate buffer byte array
		Packet pack = new Packet(src, dest, 0, msg);//Create a new packet, using this source, the destination provided
												//and the message
		buf = serialize(pack);					//serialize this packet object into a byte array
		InetAddress address = InetAddress.getByName("localhost");	//get the address
		DatagramPacket packet = new DatagramPacket(buf, buf.length, address, 2004);	//create a new datagramPacket.  
												//The last int is the port to send this packet, and in this case
												//this is always node 1 for client 1
		requestSocket.send(packet);				//send this packet through the socket
	}
	/**----------------------------------------
	 * @author Edward L Albrecht
	 * Method:	serialize(...)
	 * Purpose:	Serialize a packet object into
	 * 			a byte[] array to be used in
	 * 			transmission
	 * Input:	The Packet object to be converted
	 * Output:	The byte[] representation of this
	 * 			Packet
	 -----------------------------------------*/
	public byte[] serialize(Packet pack) throws IOException{
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(pack);
		return b.toByteArray();
	}
	/**----------------------------------------
	 * @author Edward L Albrecht
	 * Method:	deserialize(....)
	 * Purpose:	Deserialize a byte[] array back into
	 * 			a Packet object, to see what the
	 * 			message received is.
	 * Input:	The byte[] array to be converted
	 * Output:	A new Packet object
	 -----------------------------------------*/
	public Packet deserialize(byte[] bytes) throws ClassNotFoundException, IOException{
		ByteArrayInputStream b = new ByteArrayInputStream(bytes);
		ObjectInputStream o = new ObjectInputStream(b);
		return (Packet) o.readObject();
	}
	/**----------------------------------------
	 * @author Edward L Albrecht
	 * Method:	main(String[] args)
	 * Purpose:	Class execution
	 * Input:	A String[] array of arguments
	 * 			from the command line. NOT USED
	 * Output:	None
	 -----------------------------------------*/
	public static void main(String[] args){
		Client1 client = new Client1("Client1", 4446);
		client.run();
	}
}
